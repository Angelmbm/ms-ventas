package com.dreamshoes.sales_ms.models;

import org.springframework.data.annotation.Id;
import java.util.Date;

public class Sale {
    @Id
    private String id;
    private String userId;
    private String productId;
    private Date date;

    public Sale(String id, String userId, String productId, Date date){
        this.id = id;
        this.userId = userId;
        this.productId = productId;
        this.date = date;
    }


    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
