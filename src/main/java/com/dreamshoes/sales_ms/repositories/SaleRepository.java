package com.dreamshoes.sales_ms.repositories;

import com.dreamshoes.sales_ms.models.Sale;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface SaleRepository extends MongoRepository<Sale, String> {
    List<Sale> findByUserId (String userId);
    List<Sale> findByProductId (String productId);
    @Query("{'date' : { $gte: ?0, $lte: ?1 } }")                 
	List<Sale> findByDateRange(LocalDate fromDate, LocalDate toDate);
}
