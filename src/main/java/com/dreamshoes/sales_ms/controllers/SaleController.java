package com.dreamshoes.sales_ms.controllers;

import com.dreamshoes.sales_ms.models.Sale;
import com.dreamshoes.sales_ms.repositories.SaleRepository;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;


@RestController
public class SaleController {

    private  final SaleRepository saleRepository;

    public SaleController(SaleRepository saleRepository){
        this.saleRepository = saleRepository;
    }


    @PostMapping("/sales")
    Sale newSale(@RequestBody Sale sale){
        sale.setDate(new Date());
        return saleRepository.save(sale);
    }

    @GetMapping("/sales")
    List<Sale> getAllSales(){
        return saleRepository.findAll();

    }

    @GetMapping("/sales/user/{userId}")
    List<Sale> userSales(@PathVariable String userId){
        return saleRepository.findByUserId(userId);
    }

    @GetMapping("/sales/product/{productId}")
    List<Sale> productSales(@PathVariable String productId){
        return saleRepository.findByProductId(productId);
    }

    @GetMapping("/sales/{one_date}/{two_date}")
    List<Sale> getSalesByDateRange(@PathVariable(value = "one_date") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fromDate, 
    		@PathVariable(value = "two_date") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate toDate){
    	List<Sale> salesByDate = saleRepository.findByDateRange(fromDate,toDate);
        return salesByDate;

    }

}
